## Fortis BNP Paribas Assignment

### Intro

This is my solution to the **bowling exercise** as described in [this kata](http://codingdojo.org/kata/Bowling/). It is an application for an opening as iOS Developer at Fortis BNP Paribas.

### Running

The code is an Xcode playground. If you run the playground, it will first execute the tests and then do a bowling game based on randomly generated scores.

The code was developed in Xcode 10.1 and not tested on other Xcode versions.

### Way of thinking

There is not much to be seen from the commit history. Since the assignment also asks to give some insight in the way of thinking, I want to clarify a bit.

I started out with the enums with associated values as a model. I first programmed a random game, and ran them until I found the first mistake (this was on one of the first runs of course).

Then I made sure the solutions were correct for this randomly generated game, by making sure that I can also run the code with predetermined rolls, using a parametrised function to decide the outcome of a throw. This was then also my first test case.

After that I added the test cases from the example. There was still some fine tuning to do at the end of the frame calculation.

Then I cleaned up the code: I made sure that the score logic was in the enum by creating the optional initialiser and made sure the tests were silent but the random game showed some output.

After sending this off I thought of a way to only have one loop in the play function, so that is a separate, interesting commit to look at (6e809ac4deccd082244d7f175dfc1b2f73e42a90).

