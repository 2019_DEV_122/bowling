import Foundation

enum Frame: Equatable {
    case score(Int,Int?)    // regular score 0...9
    case spare(Int?)        // AV is score for next ball
    case strike(Int?,Int?)  // AV is score for next ball and ball after
    case nothing            // not thrown a ball yet
    
    enum FrameState {
        case consumed
        case propagate
    }
    
    init?(_ b1: Int) {
        if b1 == 10 {
            self = .strike(nil,nil)
        } else {
            return nil
        }
    }
    
    init(_ b1: Int, _ b2: Int) {
        if b1 + b2 == 10 {
            self = .spare(nil)
        } else {
            self = .score(b1,b2)
        }
    }
    
    mutating func addScore(_ newb1: Int) -> FrameState {
        switch self {
        case .spare(let selfb):
            assert(selfb == nil)
            self = .spare(newb1)
            return .propagate
        case .strike(let selfb1, let selfb2):
            if selfb1 == nil {
                self = .strike(newb1, nil)
                return .propagate
            }
            assert(selfb2 == nil)
            self = .strike(selfb1, newb1)
            return .propagate
        case .nothing:
            if newb1 == 10 {
                self = .strike(nil,nil)
                return .consumed
            } else {
                self = .score(newb1,nil)
                return .consumed
            }
        case .score(let selfb1, let selfb2):
            assert(selfb2 == nil)
            if selfb1 + newb1 == 10 {
                self = .spare(nil)
            } else {
                self = .score(selfb1, newb1)
            }
            return .consumed
        }
    }
    
    func value() -> Int? {
        switch self {
        case .spare(let x):
            return (x != nil) ? 10+x! : nil
        case .strike(let x, let y):
            return (x != nil && y != nil) ? 10 + x! + y! : nil
        case .score(let x, let y):
            return (y != nil) ? x + y! : nil
        case .nothing:
            return nil
        }
    }
}

func play(bowl: ((Int, Bool) -> (Int)), doPrint: Bool) -> Int {
    // start with empty frames
    var frames = Array(repeating: Frame.nothing, count: 10)
    var value: Int = 0
    for i in 0..<frames.count {
        if doPrint {
            print("Frame \(i+1)")
        }
        var rest = 10
        while frames[i].value() == nil {
            let b = bowl(rest, doPrint)
            rest = rest - b
            var j = i
            var p = Frame.FrameState.propagate
            for j in i..<frames.count where p == .propagate {
                var fj = frames[j]
                p = fj.addScore(b)
                frames[j] = fj
            }
        }
        value = value + frames[i].value()!
        if doPrint {
            print("  ** Result \(i+1) = \(frames[i].value()!)")
        }
    }
    return value
}


// First some tests:
func testBowl(bowls: [Int]) -> ((Int,Bool)->(Int)) {
    var index = 0
    return { _,_ in
        let bowled = bowls[index]
        index = index + 1
        return bowled
    }
}

let tf1 = testBowl(bowls: [10,6,2,7,1,0,1,2,2,2,3,8,0,9,0,7,3,2,5])
let testscore1 = play(bowl: tf1, doPrint: false)
assert(testscore1 == 80)

let tf2 = testBowl(bowls: [10,10,10,10,10,10,10,10,10,10,10,10])
let testscore2 = play(bowl: tf2, doPrint: false)
assert(testscore2 == 300)

let tf3 = testBowl(bowls: [9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0])
let testscore3 = play(bowl: tf3, doPrint: false)
assert(testscore3 == 90)

let tf4 = testBowl(bowls: [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5])
let testscore4 = play(bowl: tf4, doPrint: false)
assert(testscore4 == 150)


// And then a random game:
func randomBowl(_ ballsLeft: Int, _ doPrint: Bool) -> Int {
    let bowled = Int.random(in: 0...ballsLeft)
    if doPrint {
        print(" Randomly bowled \(bowled)")
    }
    return bowled
}

let score = play(bowl: randomBowl, doPrint: true)
print("\n... and scored \(score) in total")
